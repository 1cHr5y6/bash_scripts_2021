# functions.sh
#!/bin/bash

# Universal command to identify file type
identify_file_type() {
    echo "Enter the name of the file/directory to identify it's type: "
    read input 
    cmd='ls -ld'
    $cmd $input 
    $cmd ${input} | ${cmd2}

    if [[ $(ls -ld $input | cut -c1) == '-' ]]; then
        echo 'Regular file type.'
    elif [[ $(ls -ld $input | cut -c1) == 'd' ]]; then
        echo 'Directory type.'
    elif [[ $(ls -ld $input | cut -c1) == 'c' ]]; then
        echo 'Character device file type.'
    elif [[ $(ls -ld $input | cut -c1) == 'b' ]]; then
        echo 'Block device file type.'
    elif [[ $(ls -ld $input | cut -c1) == 's' ]]; then
        echo 'Local domain socket file type.' 
    elif [[ $(ls -ld $input | cut -c1) == 'p' ]]; then
        echo 'Named pipe device type.'
    elif [[ $(ls -ld $input | cut -c1) == 'l' ]]; then
        echo 'Symbolic link type.'
    else
        echo 'File type not found.'

    fi

}






